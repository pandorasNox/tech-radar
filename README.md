# tech-radar
Personal tech-radar to keep track of my technology choices / decisions.

Inspired by http://thoughtworks.com/radar/
https://www.thoughtworks.com/radar/faq

## quadrants examples
thoughtworks:
- tools
- techniques
- platforms
- languages & frameworks

zalando:
- Frameworks
- Infrastructure
- Data Management
- Languages

## My quadrants
- Languages
- Frameworks
- Infrastructure / Tools / platforms
- techniques
- Data Management

## ring draft classification

Adapted:
- docker
- nodejs
- reactjs / react-native
    - create-react-app / create-react-native-app
- redux
- mongodb
- MySQL
- nginx
- HAProxy
- Express.JS
- nightwatch.js
- selenium / grid
- API driven
- static site generators / JAMStack
- jest
- slack
- jira
- trello
- digitalocean
- styled-components
- JavaScript (ES5, ES6, ES2016, ES2017)
- Yarn

Trial:
- rabbitmq
- Consumer-Driven Contracts
- php
- symfony
- Gatsby.js / react-static
- TensorFlow
- Postgres

ASSESS:
- Elixir
- gocd
- Kontena
- RxJS
- elm
- Ansible
- Kubernetes
- Phoenix
- serverless (functions)
- Enzyme
- graphql
- GoogleChrome/puppeteer
- Prometheus
- Laravel
- Lumen
- headless xyz (CMS)
- pipeline as code
- webtask.io
- AWS Lambda
- Pyhton 3
- rkt (rockit)
- vagrant
- HashiCorp Vault
- Nodemailer
- Keycloak
- auth0.com
- Koa
- LoopBack
- serverless architectures
- Electron

examine:
- Consul (!!)
- pa11y (!)
- micro frontend
- ElasticSearch vs Solr
- tailor.js
- next.js
- Scikit-learn
- Serverless Framework
- Talisman
- Terraform
- Android-x86
- bottledwater-pg
- claudiajs
- Spinnaker
- OpenTracing
- Amazon API Gateway
- Kafka / Kafka Streams
- Ember.js
- ReactiveX
- caffe.berkeleyvision.org
- Keras
- Kong (getkong.org)
- mesos.apache.org
- goss / serverspec
- rocket.chat
- openstack

Hold:
- golang
- rust
- metalsmith

avoid:
- magento 1

## ToDo:
- from draft to final first version
- arraging this as volums like thoughtworks
- maybe introduce "watch/observe" ring
- experiement templates ??!!
